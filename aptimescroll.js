'use strict';

angular.module('ap.timescroll', [])

  .directive('aptimescroll', function($filter, $interval, $timeout) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        ngChange: '&',
        color: '@',
        whitefont: '@'
      },
      replace: true,
      templateUrl: 'bower_components/aptimescroll/templates/aptimescroll.html',
      link: function($scope, element, attrs) {

        var middle = element.children('.timescrollmiddle');
        var container = middle.children('.timescrollContainer');
        var step = 56, actPos = 0, maxPos = -1516;

        $scope.times = [
          {nr: 600, hours: '06', minutes: '00'},
          {nr: 630, hours: '06', minutes: '30'},
          {nr: 700, hours: '07', minutes: '00'},
          {nr: 730, hours: '07', minutes: '30'},
          {nr: 800, hours: '08', minutes: '00'},
          {nr: 830, hours: '08', minutes: '30'},
          {nr: 900, hours: '09', minutes: '00'},
          {nr: 930, hours: '09', minutes: '30'},
          {nr: 1000, hours: '10', minutes: '00'},
          {nr: 1030, hours: '10', minutes: '30'},
          {nr: 1100, hours: '11', minutes: '00'},
          {nr: 1130, hours: '11', minutes: '30'},
          {nr: 1200, hours: '12', minutes: '00'},
          {nr: 1230, hours: '12', minutes: '30'},
          {nr: 1300, hours: '13', minutes: '00'},
          {nr: 1330, hours: '13', minutes: '30'},
          {nr: 1400, hours: '14', minutes: '00'},
          {nr: 1430, hours: '14', minutes: '30'},
          {nr: 1500, hours: '15', minutes: '00'},
          {nr: 1530, hours: '15', minutes: '30'},
          {nr: 1600, hours: '16', minutes: '00'},
          {nr: 1630, hours: '16', minutes: '30'},
          {nr: 1700, hours: '17', minutes: '00'},
          {nr: 1730, hours: '17', minutes: '30'},
          {nr: 1800, hours: '18', minutes: '00'},
          {nr: 1830, hours: '18', minutes: '30'},
          {nr: 1900, hours: '19', minutes: '00'},
          {nr: 1930, hours: '19', minutes: '30'},
          {nr: 2000, hours: '20', minutes: '00'},
          {nr: 0, hours: '00', minutes: '00', now: true},
        ];

        var setTimeNow = function(cb){
          var d = new Date();
          var h = d.getHours();
          var m = d.getMinutes();

          if(h < 10){h = '0'+h;}
          if(m < 10){m = '0'+m;}
          var nr = parseInt(String(h)+String(m));

          var items = $filter('filter')($scope.times, {now: true});

          if(items.length){
            var index = $scope.times.indexOf(items[0]);
            if(((String(m) == '00') || (String(m) == '30')) && ((nr >= 600) && (nr <= 2000))){
              $scope.times.splice(index, 1);
            }else{
              $scope.times[index] = {nr: nr, hours: h, minutes: m, now: true}
            }
          }else{
            if(((String(m) == '00') || (String(m) == '30')) && ((nr >= 600) && (nr <= 2000))){
              //Es muss keine Zeit hinzugefügt werden
            }else{
              $scope.times.push({nr: nr, hours: h, minutes: m, now: true});
            }
          }

          if(cb !== undefined) {
            cb(nr);
          }
        };

        var scrollToPos = function($index){
          var left =  (($index * step) - (middle[0].offsetWidth / 3));
          left = ((left - (left % step) + step) * -1);
          if((left >= maxPos) && (left <= 0)){
            actPos = left;
            container.css({'left': actPos+'px'});
          }
        };

        function compare(a,b) {
          if (a.nr < b.nr)
            return -1;
          if (a.nr > b.nr)
            return 1;
          return 0;
        }

        function refresh(nr) {
          $scope.ngModel = nr;
          $timeout($scope.ngChange, 0);
          actPos = 0;

          var tmpOrd = angular.copy($scope.times);
          tmpOrd.sort(compare);
          var index = tmpOrd.indexOf($filter('filter')(tmpOrd, {nr: $scope.ngModel})[0]);
          $timeout(function(){
            scrollToPos(index);
          }, 100);
        }

        function getTimeNr() {
          var d = new Date();
          var h = d.getHours();
          var m = d.getMinutes();

          if(h < 10){h = '0'+h;}
          if(m < 10){m = '0'+m;}
          return parseInt(String(h)+String(m));
        }

        // Hard-Reload
        setTimeNow(function(nr){
          refresh(nr)
        });

        // Intervall-Reload 30sec
        var refreshInterval = $interval(function () {
          if (getTimeNr() === ($scope.ngModel + 1)) { // Aktuelle Zeit ausgewählt
            setTimeNow(function(nr){
              refresh(nr)
            });
          } else { // Zeit in der Vergangenheit ausgewählt
            refresh($scope.ngModel);
          }
        }, 60000);

        $scope.setTime = function(item, $index){
          if(item.now){
            setTimeNow(function(nr){
              $scope.ngModel = nr;
              $timeout($scope.ngChange, 0);
              scrollToPos($index);
            });
          }else{
            setTimeNow(); // Aktuallisiert den Button mit der aktuellen Zeit
            $scope.ngModel = item.nr;
            $timeout($scope.ngChange, 0);
            scrollToPos($index);
          }
        };

        //SCROLLHANDLER-------------------------------------------------------------------------------------
        $scope.scrollRight = function(){
          actPos = actPos - step;
          if(actPos < maxPos){ actPos = actPos + step}
          container.css({'left': actPos+'px'});
        };

        $scope.scrollLeft = function(){
          actPos = actPos + step;
          if(actPos > 0){actPos = 0;}
          container.css({'left': actPos+'px'});
        };

        var promise;
        $scope.mouseDown = function(dir) {
          promise = $interval(function () {
            if (dir === "left")
              $scope.scrollLeft();
            else
              $scope.scrollRight();
          }, 100);
        };

        $scope.mouseUp = function () {
          $interval.cancel(promise);
        };

        function onWheel(e) {
          e.preventDefault();

          if(e.deltaY > 0){
            $scope.scrollRight();
          }else{
            $scope.scrollLeft();
          }
        }

        middle[0].addEventListener('DOMMouseScroll', onWheel, false ); // For FF and Opera
        middle[0].addEventListener('mousewheel', onWheel, false ); // For others

        $scope.$on('$destroy', function(){
          middle[0].removeEventListener('DOMMouseScroll', onWheel, false ); // For FF and Opera
          middle[0].removeEventListener('mousewheel', onWheel, false ); // For others
          $interval.cancel(refreshInterval);
        });

        //SCROLLHANDLER ENDE--------------------------------------------------------------------------------------

      }
    };
  });

